"""
ref https://stackoverflow.com/a/69361010/248616

python -m pipenv install   webdriver-manager selenium selenium-wire
"""

from seleniumwire import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.service import Service

svc    = Service(ChromeDriverManager().install())
driver = webdriver.Chrome(service=svc)

driver.get('https://epco.taleo.net/careersection/alljobs/jobsearch.ftl?lang=en')

for req in driver.requests:
  if req.response:
    print(
      req.url,
      req.headers,
      req.response.status_code,
      req.response.headers
    )

req_d__list = [req.__dict__ for req in driver.requests if req.response ]
req_d__list2 = []
req_d__list3 = []
for req in req_d__list:
  req_d_all = {k:v for k,v in req.items() }
  req_d__list2.append(req_d_all)

  for k,v in req.items():
    is_xhr = 'application/json' in req['response'].headers['content-type']  # xhr CHECKBOX-alike in browser's networK@inspector
    if not is_xhr: continue

    try:    resbody = req['response']._body.decode()
    except: resbody = req['response']._body

    req_d = {
      'method'  : req['method'],
      'url'     : req['url'],
      'headers' : req['headers'],
      '_body'   : req['_body'].decode(),

      'response': {
        'status_code' : req['response'].status_code,
        'resbody'     : resbody,
        'headers'     : req['response'].headers,
      },
    }
    req_d__list3.append(req_d)

import json
print('---')
print( json.dumps(req_d__list2, indent=2, default=str) )
print('---')
print( json.dumps(req_d__list3, indent=2, default=str) )
